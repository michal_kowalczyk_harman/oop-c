#include "triangle.hpp"

Triangle::Triangle(int aBasis, int aHeight): basis(aBasis), height(aHeight)
{
}

double Triangle::area()
{
  return 0.5*basis*height;
}
