#include "catClass.hpp"
#include "iostream"

Cat::~Cat()
{
  std::cout << "Cat destructor" << std::endl;
}

Cat::Cat()
{
  std::cout << "Cat constructor" << std::endl;
}
