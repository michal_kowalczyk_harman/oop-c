#include "fighters.hpp"

std::string Fighter::getName()
{
  return name;
}

int Fighter::hit(int damage)
{
  health -= damage;
  return health;
}

void Fighter::setName(std::string name)
{
  this->name = name;
}

void Fighter::setHealth(int health)
{
  this->health = health;
}

void Fighter::setDamageAttack(int damageAttack)
{
  this->damageAttack=damageAttack;
}

int Fighter::getDamage()
{
  return damageAttack;
}