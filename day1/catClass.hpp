#ifndef __CATCLASS_HPP__
#define __CATCLASS_HPP__

class Cat
{
  public:
  virtual void meow() = 0;
  virtual ~Cat();
  Cat();
};
#endif