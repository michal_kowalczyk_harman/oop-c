#include <iostream>
#include "nevaClass.hpp"

void Neva::meow()
{
  std::cout << "Neva meow" << std::endl;
}

Neva::Neva()
{
  std::cout << "Neva constuctor" << std::endl;
}

Neva::~Neva()
{
  std::cout << "Neva destructor" << std::endl;
}