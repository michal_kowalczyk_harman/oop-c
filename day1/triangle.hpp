#include "shape.hpp"
#ifndef __TRIANGLE_HPP__
#define __TRIANGLE_HPP__

class Triangle : public Shape
{
  int basis;
  int height;

  public:
  Triangle(int aBasis, int aHeight);
  double area();
};

#endif