#include "shape.hpp"
#ifndef __RECT_HPP__
#define __RECT_HPP__
class Rectangle : public Shape
{
  int basis;
  int height;

  public:
  Rectangle(int aBasis, int aHeight);
  double area();
};
#endif