#include "fighters.hpp"
#include <iostream>

int main()
{
  Fighter fighter1;
  Fighter fighter2;
  std::string tempStr;
  int intTmp;

  std::cout << "Put name first fighter: ";
  std::cin >> tempStr;
  fighter1.setName(tempStr);
  std::cout << "Put health first fightera: ";
  std::cin >> intTmp;
  fighter1.setHealth(intTmp);
  std::cout << "Put damage first fighter: ";
  std::cin >> intTmp;
  fighter1.setDamageAttack(intTmp);

  std::cout << std::endl << "put name second fighter: ";
  std::cin >> tempStr;
  fighter2.setName(tempStr);
  std::cout << "Put health second fighter: ";
  std::cin >> intTmp;
  fighter2.setHealth(intTmp);
  std::cout << "put damage second fighter: ";
  std::cin >> intTmp;
  fighter2.setDamageAttack(intTmp);

  std::cout << std::endl << std::endl << "***FIGHT***" << std::endl << std::endl;
  int turn = 0;
  while (true)
  {
    ++turn;
    std::cout << "turn " << turn << std::endl;
    if (fighter1.hit(fighter2.getDamage()) <=0 )
    {
      std::cout << fighter1.getName() << " was killed by " << fighter2.getName() << std::endl;
      break;
    }
    if (fighter2.hit(fighter1.getDamage()) <=0 )
    {
      std::cout << fighter2.getName() << " was killed by " << fighter1.getName() << std::endl;
      break;
    }
  }
}