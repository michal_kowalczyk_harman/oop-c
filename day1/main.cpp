#include <iostream>
#include "shape.hpp"
#include "triangle.hpp"
#include "rect.hpp"

int main()
{
  int basisShape = 5;
  int heightShape = 5;
  Triangle triangle{basisShape, heightShape};
  Rectangle rect{basisShape, heightShape};

  std::cout << "triangle: " << triangle.area() << std::endl;
  std::cout << "rectangle: " << rect.area() << std::endl;
}