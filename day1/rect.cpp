#include "rect.hpp"

Rectangle::Rectangle(int aBasis, int aHeight): basis(aBasis), height(aHeight)
{
}

double Rectangle::area()
{
  return static_cast<double>(basis*height);
}
