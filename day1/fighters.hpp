#include <string>

class Fighter
{
  private:
  std::string name;
  int health;
  int damageAttack;

  public:
  std::string getName();
  int hit(int damage);
  void setName(std::string name);
  void setHealth(int health);
  void setDamageAttack(int DamageAttack);
  int getDamage();
};