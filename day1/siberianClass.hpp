#ifndef __SIBERIANCLASS_HPP__
#define __SIBERIANCLASS_HPP__
#include "catClass.hpp"

class Siberian : public Cat
{
  public:
  int weight;
  void meow() override;
  ~Siberian() override;
  Siberian();
};
#endif