#ifndef __DAUGHTERCLASS_HPP__
#define __DAUGHTERCLASS_HPP__
#include "motherClass.hpp"

class Daughter : public Mother
{
  public:
  void display();
};
#endif