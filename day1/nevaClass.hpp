#ifndef __NEVACLASS_HPP__
#define __NEVACLASS_HPP__
#include "catClass.hpp"

class Neva : public Cat
{
  public:
  int weight;
  void meow() override;
  ~Neva() override;
  Neva();
};
#endif