#include <iostream>
#include "siberianClass.hpp"

void Siberian::meow()
{
  std::cout << "Siberian meow" << std::endl;
}

Siberian::Siberian()
{
  std::cout << "Siberian constructor" << std::endl;
}

Siberian::~Siberian()
{
  std::cout << "Siberian destructor" << std::endl;
}